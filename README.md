# Remote Video Trigger

Raspberry Pi-based remote video trigger solution.

> **Warning**: This solution works with RPi OS Buster. It does not work on RPi OS Bullseye (current stable) yet. TODO: Switch from using omxplayer to vlc for video playback.

## Setting up Raspberry Pi

This should work on all Raspberry Pi mini-computers with the Raspberry Pi OS (version Buster or older) installed. Follow the instructions below.

1. Download and install [Raspberry Pi Imager](https://www.raspberrypi.com/software/).

1. Get an, at least, 8 GB Class 10 SD card and use the Raspberry Pi Imager to flash the latest available **Raspberry Pi OS Lite** on the SD card. Open Raspbery Pi Imager, choose **Raspberry Pi OS (other)** and pick the **Raspberry Pi OS Lite (32-bit)** option.

    > **Important**: You should install the LITE version of Raspberry Pi OS. No desktop, no cry.

1. You need to enable the correct settings for the composite output before connecting to the CRT TV module. Re-insert the SD card into the card reader slot of your computer and navigate your file browser to the `/boot` volume. Edit the `config.txt` file and uncomment the `sdtv_mode=2` line or run the script below.

    ```
    sudo sed -i "s/#sdtv_mode=2/sdtv_mode=2/" /boot/config.txt
    ```

    In case things do not look too good (e.g. black and white), you might want to look at other [possible PAL video settings here](https://www.elinux.org/RPiconfig#Video). It could be that the CRT TV modules are from another country and the PAL settings there are different, thus requiring some tuning.

1. Connect CRT TV or HDMI monitor and keyboard to your Raspberry Pi. Insert the flashed SD card into the SD card slot of the Raspberry Pi and power it up.

1. Log in using the default username `pi` and the password `raspberry`. Make sure that your Pi supports WiFi - use a WiFi USB dongle if that is not the case. Use the `raspi-config` configuration tool to connect your pi to the WiFi network. Ethernet cable is also an option, of course.

    ```
    sudo raspi-config
    ```

1. Make sure you have internet connection. This can be achieved either by connecting your router to the wider area network. You can also share internet connection from your phone or via an ethernet cable directly. Run the following command to see if your Pi has acquired an IP address.

    ```
    hostname -I
    ```

## Setting up Remote Video Trigger

1. Update your Raspbian OS system.

    ```
    sudo apt update
    sudo apt upgrade -y
    ```

1. Install git.

    ```
    sudo apt install git -y
    ```

1. Clone the repository and move in it.

    ```
    git clone https://gitlab.com/kriwkrow/remote-video-trigger.git
    cd remote-video-trigger
    ```

1. Run the setup script.

    ```
    sudo ./setup
    ```

1. Once more check the IP address of your device and write it down.

    ```
    hostname -I
    ```

1. Reboot the system and wait for circa 30 seconds. The video should start playing automatically.

    ```
    sudo reboot
    ```

## Remote Control

Assuming that you have set up a Raspberry Pi with IP addresse `192.168.0.10`, you can use `curl` from a computer connected to the same network to test if it works. Check the [Setting up Static IP Address](#setting-up-static-ip-address) section it you want to set up a static IP on your Pi.

- Use the `next` endpoint to trigger next video.

    ```
    curl "http://192.168.0.10/next"
    ```

- Use the `prev` endpoint to trigger previous video.

    ```
    curl "http://192.168.0.10/prev"
    ```

- Use the `load?video=video_name` endpoint to play a specific video by its name. File extension can be omitted.

    ```
    curl "http://192.168.0.10/load?video=video-d"
    ```

- [TODO] Use the `play?id=N` endpoint to play a specific video by its order in a list.

- [TODO] Use the `exit` endpoint to stop the video and show the console.

## Video Encoding

Use FFmpeg to encode videos. So far this command has proven to provide good results.

```
ffmpeg -i mechanical-k_WebcamNoise_SlidingGlass.mp4 -vf scale=1080x720 \
  -c:v libx264 -profile:v baseline -preset slow -crf 25 \
  -tune zerolatency -pix_fmt yuv420p \
  -c:a copy -f mp4 video-d.mp4
```

## Possible Problems With Time

Make sure that time is correct using the `date` command. If the date is wrong, make sure that you are connected to the network (wired or wireless). If you are, it might be that the network requires you to use its NTP time server. Open the `/etc/systemd/timesyncd.conf` file.

```
sudo nano /etc/systemd/timesyncd.conf
```

Look for the `[Time]` part and change the `#NTP=` line as follows. You should use the NTP server that is specific to your local network. The server `ntp.example.com` below is only an example.

```
NTP=npt.example.com
```

Press `CTRL` + `X`, then hit `Y` to confirm that you want to save the changes and hit `RETURN` to confirm that you want to write to the same file you just opened. Reboot the system for the changes to take effect.

```
sudo reboot
```

After reboot, log in and confirm that date is what you expect it to be.

```
date
```

## Setting up Static IP Address

This solution assumes that you will be using a wireless network router or a router with the needed amount of LAN ethernet ports. You should be able to look up the instruction manual of the router you have and set up a few things.

1. Set the IP address of the router (gateway) to `192.168.0.1`.

1. Set the subnet mask to `255.255.255.0`.

1. Restart the router.

    The router will probably come with DHCP (automatic IP address assignment) activated. What you want is static IP addresses for your Raspberry Pi's. Follow the steps below to configure a static IP address per Raspberry Pi.

1. Edit the file dhcpcd.conf file.

    ```
    sudo nano /etc/dhcpcd.conf
    ```

1. Find the **Example static IP configuration** section and update it as follows.

    ```
    interface eth0
    static ip_address=192.168.0.10/24
    static routers=192.168.0.1
    static domain_name_servers=8.8.8.8 8.8.4.4
    nohook resolv.conf
    ```

    > Note that for each Pi you want a different static ip_address, such as `192.168.0.20`, `192.168.0.10` and so on.

1. Use the key combination `CTRL` + `X`, confirm that you want to save changes with `Y` and hit `RETURN` once more to confirm the same file name. Reboot your Pi.

    ```
    sudo reboot
    ```

1. Confirm that the IP address you just set.

    ```
    hostname -I
    ```   

## Disable Video Service

> Yeah, I want to change some things, but I can't see the console while the video is playing.

OK, here is the trick. You will have to type some commands without seeing them (like in hacker movies).

1. Wait till your Pi boots up and video starts playing.

1. Enter your user name (e.g. `pi`).

1. Enter your password (e.g. `********f`)

1. Enter the following command to stop the video service.

    ```
    sudo systemctl stop rvt.service
    ```

1. You should see the console now. Video will show up after reboot again. If you want to prevent that, here is another command you have to enter (and now you can type it while looking at it).

    ```
    sudo systemctl disable rvt.service
    ```

1. Now, you changed your mind, and you want the video to start on boot again. No problem. Run this.

    ```
    sudo systemctl enable rvt.service
    ```

1. Reboot and video is back!

    ```
    sudo reboot
    ```

## Authors

Krisjanis Rijnieks.

## License

Attribution-NonCommercial 2.0 Generic ([CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/)). Read the legal text in the [LICENSE](LICENSE) file.

If you want to use this in a commercial context, please reach out to me (krisjanis [at] rijnieks [dot] com) and we can negotiate a reasonable license fee.
