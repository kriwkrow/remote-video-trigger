#!/bin/bash

OMXPLAYER_DBUS_ADDR="/tmp/omxplayerdbus.pi"
OMXPLAYER_DBUS_PID="/tmp/omxplayerdbus.pi.pid"
export DBUS_SESSION_BUS_ADDRESS=`cat $OMXPLAYER_DBUS_ADDR`
export DBUS_SESSION_BUS_PID=`cat $OMXPLAYER_DBUS_PID`

[ -z "$DBUS_SESSION_BUS_ADDRESS" ] && { echo "Must have DBUS_SESSION_BUS_ADDRESS" >&2; exit 1; }

status=`dbus-send --print-reply --reply-timeout=500 --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play`
echo ${status}
